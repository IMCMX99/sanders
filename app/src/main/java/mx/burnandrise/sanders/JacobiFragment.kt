package mx.burnandrise.sanders

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_jacobi.*
import java.util.*

class JacobiFragment : Fragment() {

    var Matrix : ArrayList<ArrayList<EditText>> = ArrayList()
    var xs : ArrayList<EditText> = ArrayList()
    lateinit var matrix : Array<DoubleArray>
    var XS : DoubleArray? = null
    var size = 0
    var nx = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_jacobi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        Start.setOnClickListener {

            var Number = isNumber(n.text.toString())
            createMatrix(Number)
            First.visibility = View.GONE
            Second.visibility = View.VISIBLE

        }
        Calculate.setOnClickListener {

            getData()
            Second.visibility = View.GONE
            Third.visibility = View.VISIBLE

        }
        si.setOnClickListener{

            createBlanks()
            Third.visibility = View.GONE
            Fourth.visibility = View.VISIBLE

        }
        no.setOnClickListener {

            CalculateValues()
            if(XS!=null){
                showValues(XS!!)
            }
            Third.visibility = View.GONE
            Fifth.visibility = View.VISIBLE

        }
        Calculate2.setOnClickListener {

            getAproximations()
            CalculateValues()
            if(XS!=null){
                showValues(XS!!)
            }
            Fourth.visibility = View.GONE
            Fifth.visibility = View.VISIBLE

        }

    }

    fun isNumber(arg : String) : Int{

        var Number = 0

        try {

            Number = arg.toInt()

        }catch (e:Exception){

            Toast.makeText(context,"Inserte Un Numero Valido",Toast.LENGTH_SHORT).show()

        }

        return Number

    }

    fun createMatrix(arg : Int){

        this.size = arg
        var un = DoubleArray(arg+1)
        matrix = Array(arg) {un}
        var i = 0
        var idv = 0

        while(i<arg){

            var List = ArrayList<EditText>()
            var text1 = "x"
            var line = LinearLayout(context)
            line.layoutParams = LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT)

            var j = 0;

            while(j<arg+1){

                var vx = EditText(context).apply {

                    if(j!=arg){

                        hint = text1 + (j+1)

                    }else{

                        hint = "="

                    }
                    inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
                    setRawInputType(InputType.TYPE_CLASS_NUMBER)
                    setTextColor(resources.getColor(R.color.IndianRed))
                    setPadding(10,10,10,10)
                    idv++

                }
                List.add(vx)
                line.addView(vx)
                j++

            }

            C.addView(line)
            Matrix.add(List)
            i++

        }

    }

    fun createBlanks(){

        var i = 0

        while(i<size){

            var text1 = "x"+(i+1)
            var line = LinearLayout(context)
            var hi = i +1
            line.layoutParams = LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT)
            var x = TextView(context).apply {

                text = text1
                textSize = 16f
                setPadding(10,10,10,10)

            }
            line.addView(x)
            var vx = EditText(context).apply {

                hint = hi.toString()
                inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
                setRawInputType(InputType.TYPE_CLASS_NUMBER)
                setTextColor(resources.getColor(R.color.IndianRed))
                setPadding(10,10,10,10)

            }
            xs.add(vx)
            line.addView(vx)
            C2.addView(line)
            i++

        }

    }

    fun getData(){

        var iterator = Matrix.iterator()
        var i = 0
        var j = 0

        while (iterator.hasNext()){

            var Line = iterator.next()
            var iteratorL = Line.iterator()
            var array = DoubleArray(Matrix.size+1)

            while(iteratorL.hasNext()){

                var Blank = iteratorL.next()

                try {

                    array[j] = Blank.text.toString().toDouble()

                }catch (e : Exception) {

                    Toast.makeText(context,"Inserte Numeros Validos",Toast.LENGTH_LONG).show()
                    break

                }

                j++

            }
            matrix.set(i,array)
            j = 0
            i++

        }

    }

    fun getAproximations(){

        XS = DoubleArray(size)

        var i = 0
        var iterator = xs.iterator()
        while (iterator.hasNext()) {

            var Number = iterator.next()
            try {

                XS!![i] = Number.text.toString().toDouble()

            }catch (e: Exception){

                Toast.makeText(context,"Inserte Numeros Validos",Toast.LENGTH_SHORT).show()

            }
            i++

        }

    }

    fun CalculateValues() {

        nx = size
        val AuxXs = ArrayList<Double>()
        var xs: DoubleArray? = null
        var oldxs: DoubleArray? = DoubleArray(nx)

        var iter = 0
        var cont = true

        while (cont) {

            xs = DoubleArray(nx)

            for (row in matrix.indices) {

                var div = 0.0

                for (column in 0 until matrix[row].size) {

                    div = matrix[row][row] * -1

                    if (column != row) {

                        if (iter != 0) {

                            if (column != matrix[row].size - 1) {

                                xs[row] += matrix[row][column] * oldxs!![column] / div

                            } else {

                                xs[row] += matrix[row][column] / (div * -1)

                            }

                        } else {

                            if (XS != null) {

                                if (column != matrix[row].size - 1) {

                                    xs[row] += matrix[row][column] * XS!![column] / div

                                } else {

                                    xs[row] += matrix[row][column] / (div * -1)

                                }

                            } else {

                                if (column != matrix[row].size - 1) {

                                    xs[row] += matrix[row][column] * 0 / div

                                } else {

                                    xs[row] += matrix[row][column] / (div * -1)

                                }

                            }

                        }

                    }

                }

            }

            AuxXs.clear()
            for (i in xs.indices) {

                AuxXs.add(xs[i])

            }

            if (iter != 0) {

                var isNegative = false
                var isNegative1 = false
                val Auxiliar = DoubleArray(nx)

                for (i in Auxiliar.indices) {

                    Auxiliar[i] = xs[i] - oldxs!![i]

                }

                val Auxiliar2 = xs
                Arrays.sort(Auxiliar)
                Arrays.sort(Auxiliar2)

                for (i in Auxiliar.indices) {

                    if (Auxiliar[i] < 0) {

                        isNegative = true

                    } else {
                        isNegative = false
                    }

                }

                for (i in Auxiliar2.indices) {

                    if (Auxiliar2[i] < 0) {

                        isNegative1 = true

                    } else {
                        isNegative1 = false
                    }

                }

                var sup = 0.0
                var inf = 0.0

                if (isNegative == true) {

                    sup = Auxiliar[0]

                } else {
                    sup = Auxiliar[Auxiliar.size - 1]
                }

                if (isNegative1 == true) {

                    inf = Auxiliar2[0]

                } else {
                    inf = Auxiliar2[Auxiliar2.size - 1]
                }

                val Condition = sup / inf

                if (Condition == 0.0) {

                    cont = false

                }

            }

            oldxs = null
            oldxs = DoubleArray(xs.size)
            var j = 0
            for (number in AuxXs) {

                oldxs[j] = number
                j++

            }
            xs = null
            iter++

        }

        if (oldxs != null) {
            XS = oldxs
        }

    }

    fun showValues(Values : DoubleArray){

        var i = 0

        while(i<Values.size){

            var text1 = "El Valor de x"+(i+1) + " es: " + Values[i]
            var line = LinearLayout(context)
            line.layoutParams = LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT)
            var x = TextView(context).apply {

                text = text1
                textSize = 16f
                setPadding(10,10,10,10)

            }
            line.addView(x)
            C3.addView(line)
            i++

        }

    }

}

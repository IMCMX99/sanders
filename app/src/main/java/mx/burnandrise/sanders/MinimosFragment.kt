package mx.burnandrise.sanders

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_minimos.*
import mx.burnandrise.sanders.res.Evaluar
import mx.burnandrise.sanders.res.MinimosCuadrados
import java.lang.Exception

class MinimosFragment : Fragment() {

    lateinit var xv : IntArray
    lateinit var fxv : IntArray
    lateinit var xval : DoubleArray
    lateinit var fxval : DoubleArray
    var xsv : ArrayList<EditText> = ArrayList()
    var fxsv : ArrayList<EditText> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_minimos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        NumberD.setOnClickListener {

            var compr : Boolean
            var num = 0
            var text = n.text.toString()

            try{

                num = text.toInt()
                compr = true

            }catch (e : Exception){

                compr = false
                Toast.makeText(context,"Inserta Un Valor Numerico",Toast.LENGTH_SHORT).show()

            }

            if(compr){

                CrearCampos(num)
                mainContainer.visibility = View.GONE
                scroll.visibility = View.VISIBLE

            }

        }
        Calculate.setOnClickListener {

            getData()
            var Function = MinimosCuadrados(xval,fxval)
            scroll.visibility = View.GONE
            contain.visibility = View.VISIBLE
            Modelo.text = Function.function
            Modeloy.text= Function.functionY

        }
        Button2.setOnClickListener {

            var value = xvalue.text.toString()
            var valuey = yvalue.text.toString()
            var x = 0.0
            var y = 0.0
            var correct : Boolean
            var corry : Boolean
            try{

                x = value.toDouble()
                correct = true

            }catch(e : Exception){

                correct = false

            }

            try{

                y = valuey.toDouble()
                corry = true

            }catch(e : Exception){

                corry = false

            }

            if(correct||corry){

                var text = "El Valor Para Y es: " + Evaluar(Modelo.text.toString(),"x",x).resultado
                var text2 = "El Valor Para X es: " + Evaluar(Modeloy.text.toString(),"y",y).resultado
                result.text = text
                resulty.text = text2

            }else{

                Toast.makeText(context,"Inserta Valores Numerico",Toast.LENGTH_SHORT).show()

            }

        }

    }

    fun CrearCampos(n:Int){

        var i = 0
        xv = IntArray(n)
        fxv = IntArray(n)
        var idv = 0

        while(i<n){

            var text1 = "x"+(i+1)
            var text2 = "y"+(i+1)
            var line = LinearLayout(context)
            var hi = i +1
            var hi2 = hi * 3
            line.layoutParams = LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT)
            var x = TextView(context).apply {

                text = text1
                textSize = 16f
                setPadding(10,10,10,10)

            }
            line.addView(x)
            var vx = EditText(context).apply {

                id = idv
                hint = hi.toString()
                xv[i] = idv
                inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
                setRawInputType(InputType.TYPE_CLASS_NUMBER)
                setTextColor(resources.getColor(R.color.IndianRed))
                contentDescription = "x$idv"
                setPadding(10,10,10,10)
                idv++

            }
            xsv.add(vx)
            line.addView(vx)
            var fx =TextView(context).apply {

                text = text2
                textSize = 16f
                setPadding(10,10,10,10)

            }
            line.addView(fx)
            var fxv = EditText(context).apply {

                id = idv
                hint = hi2.toString()
                fxv[i] = idv
                inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
                setRawInputType(InputType.TYPE_CLASS_NUMBER)
                setTextColor(resources.getColor(R.color.IndianRed))
                contentDescription = "x$idv"
                setPadding(10,10,10,10)
                idv++

            }
            fxsv.add(fxv)
            line.addView(fxv)
            C.addView(line)
            i++

        }

    }

    fun getData(){

        var x = xv.size
        xval = DoubleArray(xv.size)
        fxval = DoubleArray(fxv.size)
        var i = 0
        var cont = true

        while (i<x&&cont){

            try{

                xval[i] = xsv.get(i).text.toString().toDouble()
                fxval[i] = fxsv.get(i).text.toString().toDouble()

            }catch (e:Exception){

                cont=false
                Toast.makeText(context,"Inserta Un Valor Numerico",Toast.LENGTH_SHORT).show()

            }

            i++

        }

    }

}
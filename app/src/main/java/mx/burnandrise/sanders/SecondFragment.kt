package mx.burnandrise.sanders


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_second.*

class SecondFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lineal.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondFragment_to_linealFragment))
        Cuadratic.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondFragment_to_cuadraticFragment))
        Newton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondFragment_to_INewtonFragment))
        Lagrange.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondFragment_to_lagrangeFragment))
        Minimos.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondFragment_to_minimosFragment))
        btnAbout.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondFragment_to_aboutFragment))

    }

}

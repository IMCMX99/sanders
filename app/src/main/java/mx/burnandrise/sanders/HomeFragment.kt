package mx.burnandrise.sanders


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        first.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_mainFragment))
        second.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_secondFragment))
        btnAbout.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_aboutFragment))
        Third.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_thirdFragment))

    }

}

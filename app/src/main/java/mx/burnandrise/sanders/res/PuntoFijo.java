package mx.burnandrise.sanders.res;

import mx.burnandrise.sanders.res.misc.IteracionPF;

import java.util.ArrayList;
import java.util.Iterator;

public class PuntoFijo {

    double x0, xn, Cond;
    ArrayList<IteracionPF> listIter = new ArrayList<IteracionPF>();
    public int Niter = 0;

    public PuntoFijo(String Function, String var, double[] Intervalos, double Tol) {

        String Derivada = new Derivar(Function).getDerivada();
        String x = "x-(("+Function+")/("+Derivada+"))";
        Function = x;

        IteracionPF iter = new IteracionPF();

        x0 = Intervalos[0];
        xn = new Evaluar(Function, var, x0).getResultado();

        Niter = 0;
        iter.setNI(Niter);
        iter.setValueOf((double)Math.round( (x0)* 1000000d )/ 1000000d);
        iter.setEv((double)Math.round( (xn)* 1000000d )/ 1000000d);
        listIter.add(iter);

        Cond = x0 - xn;

        if(Cond<0){

            Cond = Cond * -1;

        }

        while (Cond > Tol){

            IteracionPF itern = new IteracionPF();

            x0 = xn;
            xn = new Evaluar(Function, var, x0).getResultado();

            Niter += 1;
            itern.setNI(Niter);
            itern.setValueOf((double)Math.round( (x0)* 1000000d )/ 1000000d);
            itern.setEv((double)Math.round( (xn)* 1000000d )/ 1000000d);
            listIter.add(itern);

            Cond = x0 - xn;
            if(Cond<0){

                Cond = Cond * -1;

            }

        }

        int List = 0;
        Iterator<IteracionPF> Ite = listIter.iterator();

        while (Ite.hasNext()) {

            IteracionPF Wt = Ite.next();

            if (List == 0) {

                System.out.println("\n Iteracion |  Valor  | Evaluacion ");
                System.out.println("     " +"\033[34m"+ Wt.getNI() + "       " + "\033[33m"+Wt.getValueOf()
                        +"\033[32m"+ "  "+Wt.getEv());
                List += 1;

            }else {

                System.out.println("     " + "\033[34m"+Wt.getNI() + "       " + "\033[33m"+Wt.getValueOf()
                        + "\033[32m"+"  "+Wt.getEv());
                List += 1;

            }

        }

        System.out.println("\033[31m"+"\nRaiz: " + xn);
        System.out.println("\033[0m");


    }

    public ArrayList<IteracionPF> getListIter() {
        return listIter;
    }
}

package mx.burnandrise.sanders.res;

public class MinimosCuadrados {

    public String Function;
    public String FunctionY;

    public MinimosCuadrados(double[] Xs, double[] Ys) {

        double[] xy = new double[Xs.length];
        double[] x2 = new double[Xs.length];

        double sumx = 0.0;
        double sumy = 0.0;
        double sumxy = 0.0;
        double sumx2 = 0.0;

        for (int i = 0; i < Xs.length; i++) {

            xy[i] = Xs[i] * Ys[i];
            x2[i] = Xs[i] * Xs[i];

        }

        for (int i = 0; i < Xs.length; i++) {

            sumx += Xs[i];
            sumy += Ys[i];
            sumxy += xy[i];
            sumx2 += x2[i];

        }

        if ( sumx < 0) {

            sumx = sumx * -1;

        }

        double m = (Xs.length*sumxy-(sumx*sumy))/(Xs.length*sumx2-(sumx*sumx));
        double b = (sumy*sumx2-sumx*sumxy)/(Xs.length*sumx2-(sumx*sumx));

        String fx = m + "*x+" + b;
        String fy = "(y-" + b + ")/(" + m + ")";

        Function = fx;
        FunctionY = fy;

    }

    public String getFunction() {
        return Function;
    }

    public void setFunction(String function) {
        Function = function;
    }

    public String getFunctionY() {
        return FunctionY;
    }

    public void setFunctionY(String functionY) {
        FunctionY = functionY;
    }
}

package mx.burnandrise.sanders.res;

import mx.burnandrise.sanders.res.misc.IteracionVI;

import java.util.ArrayList;

public class ValorIntermedio {

    public ArrayList<IteracionVI> Iteraciones = new ArrayList<>();

    public ValorIntermedio(String Funcion, String var, double[] Intervalos, double Tolerancia) {

        double fa = Intervalos[0];
        double fb = Intervalos[1];

        double pn = (Intervalos[0] + Intervalos[1]) / 2;
        int Iteracion = 1;

        while (true){

            double fn;
            IteracionVI Iter = new IteracionVI();

            if (Iteracion == 1) {

                Iter.setIteracion(Iteracion);
                Iter.setAn(Intervalos[0]);
                Iter.setBn(Intervalos[1]);
                Iter.setPn(pn);
                fn = Math.round(new Evaluar(Funcion, var, pn).getResultado()*1000000d)/1000000d;
                Iter.setFn(fn);
                Iteraciones.add(Iter);

                double Fa = new Evaluar(Funcion,var,Intervalos[0]).getResultado() * fn;

                if ( Fa > 0) {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }


                    }

                } else {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb= pn;

                        }


                    }


                }

                Iteracion ++;

            } else {

                Iter.setIteracion(Iteracion);
                Iter.setAn(fa);
                Iter.setBn(fb);

                pn = Math.round(((fa + fb)/2)*1000000d)/1000000d;

                Iter.setPn(pn);
                fn = Math.round(new Evaluar(Funcion, var, pn).getResultado()*1000000d)/1000000d;
                Iter.setFn(fn);
                Iteraciones.add(Iter);

                double Fa = new Evaluar(Funcion,var,fa).getResultado() * fn;

                if ( Fa > 0) {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }


                    }

                } else {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb= pn;

                        }


                    }


                }
                Iteracion++;

                if(Iteracion>1000000){

                    System.out.println("La Ecuacion En Los Intervalos Seleccionados No Cumple Con Las Condiciones " +
                            "Para Ser Resuelta Por Este Metodo");
                    break;

                }

            }

        }

    }

    public ArrayList getIteraciones() {
        return Iteraciones;
    }

}


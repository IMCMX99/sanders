package mx.burnandrise.sanders.res;

import java.util.ArrayList;
import java.util.Arrays;

public class MetododeJacobi {

    public double[][] matrix = null;
    public boolean Status = false, Closed = false;
    int Option,nx;
    double[] XS = null;

    public MetododeJacobi(double[][] matrix, double[] xs,boolean useAproximations) {


    }

    private void CalculateValues(){

        ArrayList<Double> AuxXs = new ArrayList<>();
        double[] xs = null;
        double[] oldxs = new double[nx];

        int iter = 0;
        boolean cont = true;

        while(cont){

            xs = new double[nx];

            for (int row = 0; row < matrix.length; row++) {

                double div = 0;

                for (int column = 0; column < matrix[row].length; column++) {

                    div = matrix[row][row]*-1;

                    if( column != row){

                        if(iter!=0){

                            if(column != matrix[row].length-1){

                                xs[row] += (matrix[row][column]*oldxs[column])/div;

                            } else {

                                xs[row] += matrix[row][column]/(div*-1);

                            }

                        } else {

                            if(XS!=null){

                                if(column != matrix[row].length-1){

                                    xs[row] += (matrix[row][column]*XS[column])/div;

                                }else{

                                    xs[row] += matrix[row][column]/(div*-1);

                                }

                            } else {

                                if(column != matrix[row].length-1){

                                    xs[row] += (matrix[row][column]*0)/div;

                                }else{

                                    xs[row] += matrix[row][column]/(div*-1);

                                }

                            }

                        }

                    }

                }

            }

            AuxXs.clear();
            for (int i = 0; i < xs.length; i++) {

                AuxXs.add(xs[i]);

            }

            if(iter!=0){

                boolean isNegative = false;
                boolean isNegative1 = false;
                double[] Auxiliar = new double[nx];

                for (int i = 0; i < Auxiliar.length; i++) {

                    Auxiliar[i] = xs[i] - oldxs[i];

                }

                double[] Auxiliar2 = xs;
                Arrays.sort(Auxiliar);
                Arrays.sort(Auxiliar2);

                for (int i = 0; i < Auxiliar.length; i++) {

                    if(Auxiliar[i]<0){

                        isNegative = true;

                    } else{
                        isNegative = false;
                    }

                }

                for (int i = 0; i < Auxiliar2.length; i++) {

                    if(Auxiliar2[i]<0){

                        isNegative1 = true;

                    } else{
                        isNegative1 = false;
                    }

                }

                double sup = 0;
                double inf = 0;

                if(isNegative==true){

                    sup = Auxiliar[0];

                }else{
                    sup = Auxiliar[Auxiliar.length-1];
                }

                if(isNegative1==true){

                    inf = Auxiliar2[0];

                }else {
                    inf = Auxiliar2[Auxiliar2.length-1];
                }

                double Condition = sup/inf;

                if (Condition==0){

                    cont = false;

                }

            }

            oldxs = null;
            oldxs = new double[xs.length];
            int j = 0;
            for(Double number : AuxXs){

                oldxs[j] = number;
                j++;

            }
            xs = null;
            iter++;

        }

        System.out.println("Calculando...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        for (int i = 0; i < oldxs.length; i++) {

            System.out.println("El Valor de X"+ (i+1) + " es: " + oldxs[i]);

        }

        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }

    public double[] getXS() {
        return XS;
    }

    public void setXS(double[] XS) {
        this.XS = XS;
    }
}

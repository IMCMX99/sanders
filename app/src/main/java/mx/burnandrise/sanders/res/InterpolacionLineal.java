package mx.burnandrise.sanders.res;

public class InterpolacionLineal {

    public String Function;

    public InterpolacionLineal(double[] Intervalos0, double[] Intervalos1) {

        double x0 = Intervalos0[0];
        double fx0 = Intervalos0[1];

        double x1 = Intervalos1[0];
        double fx1 = Intervalos1[1];

        double sup = fx1 - fx0;
        double inf = x1 - x0;

        double div = sup / inf;

        String fx = Double.toString(fx0) +"+" + Double.toString(div) + "*(x-" + Double.toString(x0) + ")";

        Function = fx;

    }

    public String getFunction() {
        return Function;
    }

    public void setFunction(String function) {
        Function = function;
    }
}

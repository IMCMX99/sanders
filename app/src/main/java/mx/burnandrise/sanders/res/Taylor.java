package mx.burnandrise.sanders.res;

import java.util.ArrayList;
import java.util.Scanner;

public class Taylor {

    private String Derivada;
    private double EvO, EvO2;
    private double P = 0.0;
    private double E = 0.0;

    public Taylor(String Funcion, String Var, int Grado, double x0, double Aprox) {

        Grado = Grado - 1;
        String[] Derivadas = new String[Grado + 3];
        Derivadas [0] = Funcion;
        double[] Values = new double[Grado+1];
        EvO2 = Math.round((new Evaluar( Funcion , Var, x0).getResultado()*1000000d))/1000000d;
        EvO = Math.round((new Evaluar( Funcion , Var, Aprox).getResultado()*1000000d))/1000000d;
        Derivada  = new Derivar(Funcion).getDerivada();
        Derivadas[1] = Derivada;

        for (int i = 0; i < Grado + 1; i++) {

            if (i < Grado +2) {

                Values[i] = Math.round((new Evaluar( Derivada , Var, x0).getResultado()*1000000d))/1000000d;

            }
            Derivada = new Derivar(Derivada).getDerivada();
            Derivadas[i+2] = Derivada ;

        }

        for (int i = 0; i < Grado + 3 ; i++) {

            if (i == 0) {

                P = EvO2;

            } else if ( i == 1) {

                P += Math.round(((Values[i-1] * ( Aprox - x0 ))*1000000d))/1000000d ;

            } else if ( i == Grado + 2) {

                E =  (((new Evaluar( Derivadas[i] , Var,( (x0+Aprox)/2 ) ).getResultado()) * (Math.pow((Aprox-x0),(i))))) / Factorial(i);
                break;

            } else {

                P += Math.round(((( Values[i-1] * (Math.pow( (Aprox - x0),i )) ) / Factorial(i))*1000000d))/1000000d ;

            }

        }

        System.out.println("\nFuncion Original: "+ "\033[34m" + Derivadas[0]  + "\033[0m" +" ; X0 = " +
                ("\033[34m" +x0) + "\033[0m" +" ; x = " + ("\033[34m" +Aprox) + "\033[0m");
        System.out.println("\nFuncion Original Evaluada en "+Aprox+":        "+("\033[36m" +EvO)+ "\033[0m" +
                "\nPolinomio de Taylor de "+(Grado + 1)+" Grado Evaluado: "+ ("\033[36m" +P) + "\033[0m" +"\nError:         " +
                "                          " + ("\033[31m" +E) + "\033[0m");

    }

    public double Factorial (int Grad) {

        double Factorial = (double) Grad;
        double Result = 1.0;

        while ( Factorial != 0 ) {

            Result = Result * Factorial;
            Factorial--;

        }

        return Result;

    }

}

package mx.burnandrise.sanders.res;

public class InterpolaciondeLagrange {

    public String Function;

    public InterpolaciondeLagrange(double[] Points, double[] Evaluations) {

        StringBuilder fx = new StringBuilder();
        boolean pos;
        int xi = 0;
        int xj = 0;

        for (int i = 0; i < Points.length; i++) {

            fx.append("((");
            pos = i  == Points.length-1;

            for (int j = 0; j < Points.length; j++) {

                if( i != j ) {

                    if (Points.length>1){

                        if (j==Points.length-1) {

                            fx.append("(x-").append(Points[xj]).append(")");

                        }else if(j<Points.length) {

                            if (pos && (j==i-1)) {

                                fx.append("(x-").append(Points[xj]).append(")");

                            }else{

                                fx.append("(x-").append(Points[xj]).append(")*");

                            }

                        }

                    } else {

                        fx.append("(x-").append(Points[xj]).append(")");

                    }

                }else{



                }

                xj++;

            }

            xj = 0;

            for (int j = 0; j < Points.length; j++) {

                if ( i != j ){

                    if (Points.length>2){

                        if(xi==0 || j==0){

                            fx.append(")/((").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")*");

                        } else if(j==Points.length-1) {

                            fx.append("(").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")");

                        } else {

                            if(pos && (j==i-1)) {

                                fx.append("(").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")");

                            } else {

                                fx.append("(").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")*");

                            }

                        }

                    }else {

                        fx.append(")/((").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")");

                    }

                    xi++;

                }else{



                }

                xj++;

            }

            if(i!=Points.length-1){

                fx.append("))*").append(Double.toString(Evaluations[i])).append("+");

            }else{

                fx.append("))*").append(Double.toString(Evaluations[i]));

            }
            xi = 0;
            xj = 0;

        }

        Function = fx.toString();

    }

    public String getFunction() {
        return Function;
    }

    public void setFunction(String function) {
        Function = function;
    }
}

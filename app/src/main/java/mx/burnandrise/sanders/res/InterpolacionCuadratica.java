package mx.burnandrise.sanders.res;

public class InterpolacionCuadratica {

    public String Function;

    public InterpolacionCuadratica(double[] Intervalos0, double[] Intervalos1, double[] Intervalos2) {

        double x0 = Intervalos0[0];
        double b0 = Intervalos0[1];

        double x1 = Intervalos1[0];
        double fx1 = Intervalos1[1];
        double b1 = (fx1-b0)/(x1-x0);

        double x2 = Intervalos2[0];
        double fx2 = Intervalos2[1];
        double aux0 = (fx2-fx1)/(x2-x1);
        double aux1 = (fx1-b0)/(x1-x0);
        double b2 = (aux0-aux1)/(x2-x0);

        String f2x = Double.toString(b0) + "+" + Double.toString(b1)+"*(x-" + Double.toString(x0) + ")+"
                + Double.toString(b2) + "*(x-" + Double.toString(x0) + ")*(x-" + Double.toString(x1) + ")";

        Function = f2x;

    }

    public String getFunction() {
        return Function;
    }

    public void setFunction(String function) {
        Function = function;
    }
}

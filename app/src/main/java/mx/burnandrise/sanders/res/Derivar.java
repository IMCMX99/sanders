package mx.burnandrise.sanders.res;

import android.content.pm.PackageInfo;
import org.lsmp.djep.djep.DJep;
import org.nfunk.jep.Node;
import org.nfunk.jep.ParseException;

public class Derivar {

    public String Derivada;

    public Derivar(String Funcion) {

        DJep d = new DJep();
        d.addStandardConstants();
        d.addStandardFunctions();
        d.addComplex();
        d.setAllowUndeclared(true);
        d.setAllowAssignment(true);
        d.setImplicitMul(true);
        d.addStandardDiffRules();

        try {

            Node node = d.parse(Funcion);
            Node Diff = d.differentiate(node, "x");
            Node Simp = d.simplify(Diff);
            this.Derivada = d.toString(Simp);

        } catch (ParseException e) {

            System.out.println("Math Error");

        }

    }

    public String getDerivada() {
        return Derivada;
    }
}

package mx.burnandrise.sanders.res;

import java.util.Scanner;

public class Expresion {

    public double ValueOf;

    public Expresion(String Funcion, double x0) {

        ValueOf = new Evaluar(Funcion, Var(), x0).getResultado();

    }

    public static String Var () {

        System.out.println("Inserte la variable a derivar");
        String Var = new Scanner(System.in).nextLine();
        boolean Continue = true;

        do {

            char[] var = Var.toCharArray();

            if (var.length==1) {

                Var = Character.toString( var[0] );
                Continue = false;

            } else {

                System.out.println("Inserte una Variable");

            }

        }while (Continue==true);

        return Var;


    }

}

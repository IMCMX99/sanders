package mx.burnandrise.sanders


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_cuadratic.*
import mx.burnandrise.sanders.res.Evaluar
import mx.burnandrise.sanders.res.InterpolacionCuadratica
import java.lang.Exception

class CuadraticFragment : Fragment() {

    var vaxs : DoubleArray = DoubleArray(2)
    var vafxs : DoubleArray = DoubleArray(2)
    var vafx3 : DoubleArray = DoubleArray(2)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cuadratic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        btnCalcular.setOnClickListener {

            var xs = xs(vx1.text.toString(),vfx1.text.toString())
            var fxs = fxs(Intera.text.toString(),Interb.text.toString())
            var x3s = xs3(vx3.text.toString(),vfx3.text.toString())

            if(xs==true&&fxs==true&&x3s==true){

                var Model = InterpolacionCuadratica(vaxs,vafxs,vafx3).function
                Container1.visibility = View.GONE
                contain.visibility = View.VISIBLE
                Modelo.text = Model

            }

        }
        Button2.setOnClickListener {

            var value = xvalue.text.toString()
            var x = 0.0
            var correct : Boolean
            try{

                x = value.toDouble()
                correct = true

            }catch(e : Exception){
                correct = false
            }

            if(correct){

                var text = "El Resultado es: " + Evaluar(Modelo.text.toString(),"x",x).resultado
                result.text = text

            }

        }
    }

    fun xs( x1 : String, x2 : String) : Boolean? {

        var re = false

        try{

            vaxs[0] = x1.toDouble()
            vaxs[1] = x2.toDouble()

            re = true

        }catch( e : Exception){

            re = false

        }

        return re

    }

    fun fxs( x1 : String, x2 : String) : Boolean? {

        var re: Boolean

        try{

            vafxs[0] = x1.toDouble()
            vafxs[1] = x2.toDouble()

            re = true

        }catch( e : Exception){

            re = false

        }

        return re

    }

    fun xs3( x1 : String, x2 : String) : Boolean? {

        var re = false

        try{

            vafx3[0] = x1.toDouble()
            vafx3[1] = x2.toDouble()

            re = true

        }catch( e : Exception){

            re = false

        }

        return re

    }

}

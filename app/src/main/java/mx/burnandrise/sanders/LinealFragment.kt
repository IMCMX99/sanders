package mx.burnandrise.sanders

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_lineal.*
import mx.burnandrise.sanders.res.Evaluar
import mx.burnandrise.sanders.res.InterpolacionLineal
import java.lang.Exception

class LinealFragment : Fragment() {

    var vaxs : DoubleArray = DoubleArray(2)
    var vafxs : DoubleArray = DoubleArray(2)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lineal, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnCalcular.setOnClickListener {

            var xs = xs(vx1.text.toString(),vfx1.text.toString())
            var fxs = fxs(Intera.text.toString(),Interb.text.toString())

            if(xs==true&&fxs==true){

                var Model = InterpolacionLineal(vaxs,vafxs).function
                Container1.visibility = View.GONE
                contain.visibility = View.VISIBLE
                Modelo.text = Model

            }

        }
        Button2.setOnClickListener {

            var value = xvalue.text.toString()
            var x = 0.0
            var correct : Boolean
            try{

                x = value.toDouble()
                correct = true

            }catch(e : Exception){
                correct = false
            }

            if(correct){

                var text = "El Resultado es: " + Evaluar(Modelo.text.toString(),"x",x).resultado
                result.text = text

            }

        }

    }

    fun xs( x1 : String, x2 : String) : Boolean? {

        var re = false

        try{

            vaxs[0] = x1.toDouble()
            vaxs[1] = x2.toDouble()

            re = true

        }catch( e : Exception){

            re = false

        }

        return re

    }

    fun fxs( x1 : String, x2 : String) : Boolean? {

        var re: Boolean

        try{

            vafxs[0] = x1.toDouble()
            vafxs[1] = x2.toDouble()

            re = true

        }catch( e : Exception){

            re = false

        }

        return re

    }

}

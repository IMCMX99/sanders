package mx.burnandrise.sanders


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_false.*
import mx.burnandrise.sanders.res.PosicionFalsa
import mx.burnandrise.sanders.res.TableDynamic
import mx.burnandrise.sanders.res.Tolerancia
import mx.burnandrise.sanders.res.misc.IteracionVI

class FalsePFragment : Fragment() {

    var header = arrayOf("Iter.","ai","bi","pi","f(pi)")
    var rows: ArrayList<Array<String>> = ArrayList()
    var Iterations : ArrayList<IteracionVI> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_false, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnCalcular.setOnClickListener {

            var condition: Boolean
            var inter = DoubleArray(2)
            var tol = 0.0

            try {

                inter[0] = Intera.text.toString().toDouble()
                inter[1] = Interb.text.toString().toDouble()

                var f = toleran.text.toString()
                var t = getString(R.string.Ten)+f

                tol = Tolerancia(t).valueOf

                condition = true

            }catch (e : Exception){

                Toast.makeText(context,getString(R.string.error), Toast.LENGTH_SHORT).show()
                condition = false

            }
            if (condition){


                var Newton = PosicionFalsa(etxFuncion.text.toString(),"x",inter,tol).iteraciones as ArrayList<IteracionVI>
                Iterations = Newton
                T.visibility = View.GONE
                F.visibility = View.GONE
                etxFuncion.visibility = View.GONE
                Int.visibility = View.GONE
                a.visibility = View.GONE
                Intera.visibility = View.GONE
                b.visibility = View.GONE
                Interb.visibility = View.GONE
                To.visibility = View.GONE
                Tole.visibility = View.GONE
                btnCalcular.visibility = View.GONE
                createTable()

            }

        }

    }

    fun createTable(){

        var tableDynamic = TableDynamic(Table,context)
        tableDynamic.addHeader(header)
        tableDynamic.addData(getIteraciones())

    }

    fun getIteraciones() : ArrayList<Array<String>>? {

        var x0 = 0.0

        var Iterator = Iterations.iterator()
        if (true) {
            while (Iterator.hasNext()){

                var Iteracion = Iterator.next()
                rows.add(arrayOf(Iteracion.iteracion.toString(),Iteracion.an.toString(),Iteracion.bn.toString()
                        ,Iteracion.pn.toString(),Iteracion.fn.toString()))
                x0 = Iteracion.pn

            }
        }

        var txt = "La Raiz es: " + x0.toString()
        txtRaiz.text = txt

        return rows

    }

}

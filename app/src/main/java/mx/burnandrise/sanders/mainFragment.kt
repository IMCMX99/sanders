package mx.burnandrise.sanders


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_main.*

class mainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnNewton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.toNewtonFragment))
        btnBiseccion.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_biseccionFragment))
        btnPsicionF.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_falsePFragment))
        btnPunto.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_puntoFFragment))
        btnSecante.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_secanteFragment))
        btnAbout.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_aboutFragment))

    }


}

package mx.burnandrise.sanders

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_third.*

class ThirdFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Jacobi.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_thirdFragment_to_jacobiFragment))
        Gauss.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_thirdFragment_to_gaussFragment))
        About.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_thirdFragment_to_aboutFragment))

    }

}
